import { Component } from '@angular/core';
import { Customer } from './app-classes/customer';
import { CustomersServiceService } from './customers-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
public customers: Customer[];
private toggleText: string = "Hide";
private show: boolean = true;
selectedCustomer:Customer=new Customer();
textValue = '';

  constructor(private customerService:CustomersServiceService){
    customerService.onEvent.subscribe(data=>this.customers=data);
  }
  onSelect(myRow){
    this.selectedCustomer = myRow.selectedRows[0].dataItem;
  }
  filterName(){
    this.customers = this.customerService.customers.filter(el => el.firstName.toUpperCase().includes(this.textValue.toUpperCase()));
  }
  public onToggle(): void {
    this.show = !this.show;
    this.toggleText = this.show ? "Hidе" : "Show";
  }
}