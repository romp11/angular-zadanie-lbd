import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Customer } from './app-classes/customer';
@Injectable({
  providedIn: 'root'
})
export class CustomersServiceService {
public onEvent: EventEmitter<any> = new EventEmitter();
public customers: Array<Customer>=[];
readonly ROOT_URL = 'http://demo6180277.mockable.io/customers'
title = 'app';
posts: any;

  constructor(private http: HttpClient){
    this.getCustomers();
  }
  getCustomers(){
    this.posts = this.http.get<Array<Customer>>(this.ROOT_URL).subscribe(
    data => {
      this.customers = data;
      this.onEvent.emit(this.customers);
    })
  }
}