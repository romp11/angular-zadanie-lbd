import { Component, Input } from '@angular/core';
import { CustomersServiceService } from '../../customers-service.service';
import { Customer } from '../../app-classes/customer';
@Component({
  selector: 'app-customer-address',
  templateUrl: './customer-address.component.html',
  styleUrls: ['./customer-address.component.css']
})
export class CustomerAddressComponent {
  @Input('selectedCustomer') selectedCustomer: Customer;
  constructor(private customerService:CustomersServiceService) { 
  }
}
