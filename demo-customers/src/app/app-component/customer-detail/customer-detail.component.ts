import { Component, Input } from '@angular/core';
import { CustomersServiceService } from '../../customers-service.service';
import { Customer } from '../../app-classes/customer';
@Component({
  selector: 'app-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.css']
})
export class CustomerDetailComponent {
  @Input('selectedCustomer') selectedCustomer: Customer;
  constructor(private customerService:CustomersServiceService) { }
}