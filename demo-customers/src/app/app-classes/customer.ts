import { Address } from "./address";
import { Document } from "./document";

export class Customer{
public id:Number;
public firstName:String;
public surname:String;
public phone:Number;
public email:String;
public bornDate:Date;
public company:String;
public gender:String;
public address:Address = new Address();
public document:Document = new Document();
}