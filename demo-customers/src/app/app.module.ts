import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, NgForm} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { AppComponent } from './app.component';
import { GridModule } from '@progress/kendo-angular-grid';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CustomerDetailComponent } from './app-component/customer-detail/customer-detail.component';
import { CustomerAddressComponent } from './app-component/customer-address/customer-address.component';
import { CustomersServiceService } from './customers-service.service';
import { PopupModule } from '../../node_modules/@progress/kendo-angular-popup';

@NgModule({
  declarations: [
    AppComponent,
    CustomerDetailComponent,
    CustomerAddressComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    GridModule,
    BrowserAnimationsModule,
    HttpClientModule,
    PopupModule,
  ],
  providers: [CustomersServiceService],
  bootstrap: [AppComponent],
})
export class AppModule { }