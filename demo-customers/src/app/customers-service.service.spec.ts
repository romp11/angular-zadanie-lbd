import { TestBed, inject } from '@angular/core/testing';
import { CustomersServiceService } from './customers-service.service';

describe('CustomersServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CustomersServiceService]
    });
  });

  it('should be created', inject([CustomersServiceService], (service: CustomersServiceService) => {
    expect(service).toBeTruthy();
  }));
});
